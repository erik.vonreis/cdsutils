def snap_read_iter(burtfile, prefix=None):
    """Read data from BURT snap file.

    This function is a generator that yields a (channel, value,
    monitor) tuple for each non-RO line in the specified burt snap
    file.  `monitor` is a bool that indicates whether the SDF monitor
    bit is set for this channel.

    If a `prefix` is given, matching prefix strings will be removed
    from the returned channel names.

    """
    inheader = True

    if prefix:
        plen = len(prefix)
    
    with open(burtfile, 'r') as f:
        # FIXME: we should maybe quickly slurp the entire file
        # into memory and then parse, so we don't have to worry
        # about the file changing underneath us as we apply
        for line in f:
            line = line.strip()
            if not line:
                continue

            if inheader:
                if line == '--- End BURT header':
                    inheader = False
                continue

            data = line.split()
            # if line is specified RO skip
            if data[0] in ['-', 'RO', 'RON']:
                continue

            # extract data
            channel = data[0]
            count = int(data[1])
            ind = 2
            if count == 1:
                # handle quoted strings
                if '"' in data[ind]:
                    for ii, s in enumerate(data[ind:]):
                        if s[-1] == '"':
                           break 
                    value = ' '.join(data[ind:ind+ii+1]).strip('"')
                    ind += ii + 1
                else:
                    value = data[ind]
                    try:
                        # try to convert the value to a float
                        value = float(value)
                    except ValueError:
                        value = value
                    ind += count
            else:
                value = data[ind:ind+count]
                ind += count

            # SDF monitoring bit
            monitor = False
            try:
                monitor = bool(data[ind])
            except IndexError:
                pass
            
            # strip leading prefix if there
            if prefix:
                index = channel.find(self.prefix)
                if index == 0:
                    channel = channel[plen:]

            yield channel, value, monitor
